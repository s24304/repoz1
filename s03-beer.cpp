#include <iostream>
#include <string>
#include <ctime>

int main()
{
    int bottles;
    int nbottles = 0;
    std::cout << "Enter the number of bottles between 99-1: \n";
    std::cin >> bottles;
    
    while (bottles > 99 || bottles < nbottles)
    {
        std::cout << "Wrong number of bottles, please enter the number between 99-1: ";
        std::cin >> bottles;
    }
    while (bottles > nbottles)
    {
        std::cout << bottles << " bottle(s) of beer on the wall, ";
        std::cout << bottles << " bottle(s) of beer. ";
        std::cout << "Take one down, pass it around, \n";
        std::cout << --bottles;
        
        if(bottles == nbottles)
        {
            std::cout << "\n No more bottles of beer on the wall, no more bottles of beer.\n"  "Go to the store and buy some more. \n";
        }
        else
        {
            std::cout << "bottle(s) of beer on the wall.";
        }
    }
    return 0;
}
