#include <iostream>


auto main(int argc, char *argv[]) -> int
{
    int n = std::atoi(argv[1]);
    if(n < 2)
    {
        std::cout << "boom";
        return 0;
    }
    else
    {
        for(int i = 0; i< n; ++i)
    {
        std::cout << (n % 5 == 0 ? "B" : "b");
        std::cout << (n % 5 == 0 ? "OO" : "oo");
        std::cout << (n % 5 == 0 ? "M" : "m");
    }

    if(n % 2 == 0)
    {
        std::cout << "!";
    }
    }
    return 0;
}

