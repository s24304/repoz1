#include <iostream>
#include <string>
#include <ctime>
int main()
{
    int num, guess;
    srand(time(0));
    num = rand() % 100 + 0;

    do
    {
        std::cout << "Zgadnij liczbę od 0 do 100 : ";
        std::cin >> guess;

        if (guess > num)
            std::cout << "Za dużo!\n\n";
        else if (guess < num)
            std::cout << "Za mało!\n\n";
        else
            std::cout << "\nGratulacje! Udało Ci się!\n";
    } while (guess != num);

    return 0;
}
