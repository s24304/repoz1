#include <stdio.h>
#include <math.h>

int decimalToSeptal(int decimalnum)
{
    int septalnum = 0, temp = 1;
    while (decimalnum != 0)
    {
    	septalnum = septalnum + (decimalnum % 7) * temp; decimalnum = decimalnum / 7;
        temp = temp * 10;
    }
    return septalnum;
}
int main()
{
    int decimalnum;
    printf("Wpisz liczbę w systemie dziesiętnym: ");
    scanf("%d", &decimalnum);
    printf("Twoja liczba w systemie siódemkowym: %d \n", decimalToSeptal(decimalnum));
    return 0;
}
