#include <iostream>
#include <string>

int main()
{
    int n;
    std::cout << "Podaj liczbę: ";
    std::cin >> n;
    for (int i = 1; i<=n; i++)
    {
        if (i % 15 == 0)
        {
            std::cout << "FizzBuzz\n" << std::endl;
        }
        else if (i % 5 == 0)
        {
            std::cout << "Buzz\n" << std::endl;
        }
        else if (i % 3 == 0)
        {
            std::cout << "Fizz\n" << std::endl;
        }
        else
        {
            std::cout << i <<"\n" << std::endl;
        }
    }
    return 0;
}
