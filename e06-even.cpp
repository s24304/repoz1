#include <stdio.h>

int main()
{
    int array[100],i,n, suma=0;

    printf("Podaj liczbę elementów: ");
    scanf("%d", &n);

    printf("Wypisz elementy: ");

    for (i = 0; i < n; i++)
    {
        scanf("%d", &array[i]);
	    if(array[i]%2==0)
		suma = suma +array[i];
	}

	printf("Suma wypisanych parzystych elementów: %d \n",suma);     
    return 0;
}
