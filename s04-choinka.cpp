#include <iostream>

int main()
{
    int n;
    std::cout << "Podaj wysokość choinki:  ";
    std::cin >> n;
    for (int i = 0; i < n; i++)
    {

        for (int t = 1; t < n - i; t++)
        {
            std::cout << ' ';
        }
        for (int t = 0; t < 2 * i + 1; t++)
        {
            std::cout << '*';
        }
        std::cout << std::endl;
    }
    return 0;
}
